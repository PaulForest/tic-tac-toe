﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A player that is controlled by a human.  Click or tap on tiles to place
/// your mark.
/// </summary>
public class HumanPlayer : AbstractPlayer
{
    /// <summary>
    /// The layer used for UI.  We'll raycast with this layer.  
    /// The inspector interface for this field uses the Layer names, rather than a LayerMask.
    /// This is made possible with Editor/LayerAttributeEditor.cs and the LayerAttribute class.
    /// <see cref="LayerAttribute"/>
    /// <see cref="LayerAttributeEditor"/>
    /// </summary>
    [SerializeField, Layer]
    public int uiLayer;

    /// <summary>
    /// Yup, this is a human
    /// </summary>
    public override PlayerTypeDef PlayerType { get { return PlayerTypeDef.Human; } }

    public bool IsListeningForInput
    {
        get { return _isListeningForInput; }
        set { _isListeningForInput = value; }
    }

    /// <summary>
    /// Resets the instance such that it's the same as if we created this from
    /// scratch.
    /// Reinitializes all data including its gameModel and gameView instances.
    /// </summary>
    public override void Reset()
    {
        _isListeningForInput = false;
    }

    protected bool _isListeningForInput = false;

    /// <summary>
    /// When it's our turn, listen for input
    /// </summary>
    protected override void OnTurnStart()
    {
        _isListeningForInput = true;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An AI agent that only contains the RandomBehaviour in its list.
/// Note that this is empty since AbstractAIPlayer by default will
/// add an instance of RandomBehaviour.  
/// </summary>
public class RandomAIPlayer : AIBehaviourPlayer
{
    
}
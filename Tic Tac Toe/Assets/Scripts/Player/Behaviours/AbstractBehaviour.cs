﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls a specific move an AI agent might choose to execute.
/// Uses a Conditional() method to determine if this behaviour is 
/// valid given the gameModel.
/// Uses an Execute() method to actually execute the move on the gameModel.
/// </summary>
[RequireComponent(typeof(AbstractPlayer))]
public abstract class AbstractBehaviour : MonoBehaviour
{
    /// <summary>
    /// Which player is making the move?
    /// </summary>
    public AbstractPlayer player;

    /// <summary>
    /// hold onto local copies of this as we'll use them all the time.
    /// </summary>
    protected GameController gameController;
    protected GameModel gameModel;

    /// <summary>
    /// Wire this up: find the player instance and keep a local reference to the 
    /// gameModel and gameController.
    /// </summary>
    protected virtual void Start()
    {
        player = GetComponent<AbstractPlayer>();
        gameController = player.gameController;
        gameModel = player.gameModel;
    }

    /// <summary>
    /// Determines if we can execute this behaviour on this current gameModel and
    /// gameController's state.
    /// If this method returns true then we should call Execute() on this instance.
    /// Note that the intention is that Conditional() should be somewhat cheaper to
    /// run than Execute(), but in all practicality the expensive stuff is in Conditional().
    /// </summary>
    /// <returns>true iff this behaviour could run on the current gameModel and gameController</returns>
    public abstract bool Conditional();

    /// <summary>
    /// Runs this behaviour on the gameModel and gameController.  
    /// </summary>
    /// <returns>true iff the behaviour can completely execute</returns>
    public abstract bool Execute();

}
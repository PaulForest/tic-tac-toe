﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Chooses a random available Tile.
/// Note that Conditional and Execute are only ever run if there's at least one available
/// tile.  
/// </summary>
public class RandomBehaviour : AbstractBehaviour
{
    public override bool Conditional()
    {
        return true;
    }

    public override bool Execute()
    {
        int x, y;
        do
        {
        } while (!gameController.MakeMove(x = Random.Range(0, GameModel.BoardWidth), y = Random.Range(0, GameModel.BoardHeight)));

        Debug.Log(string.Format("RandomBehaviour.Execute(): ({0},{1})", x, y));

        return true;
    }
}
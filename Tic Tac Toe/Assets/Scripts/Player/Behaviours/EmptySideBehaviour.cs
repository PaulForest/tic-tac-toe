﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The player plays in a middle square on any of the 4 sides.
/// -- https://en.wikipedia.org/wiki/Tic-tac-toe
/// </summary>
public class EmptySideBehaviour : AbstractBehaviour
{
    int candidateIndex;
    readonly int[] sides = new int[4] { 1, 3, 5, 7 };

    public override bool Conditional()
    {
        foreach (int index in sides)
            if (null == gameModel[index].owner)
            {
                candidateIndex = index;
                return true;
            }

        return false;
    }

    public override bool Execute()
    {
        int x, y;
        gameModel.IndexToPosition(candidateIndex, out x, out y);

        Debug.Log(string.Format("EmptySideBehaviour.Execute(): ({0},{1})", x, y));

        return gameController.MakeMove(x, y);
    }
}
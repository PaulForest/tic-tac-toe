﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The player plays in a corner square.
/// -- https://en.wikipedia.org/wiki/Tic-tac-toe
/// </summary>
public class EmptyCornerBehaviour : AbstractBehaviour
{
    int candidateIndex;
    readonly int[] corners = new int[4] { 0, 2, 6, 7 };

    public override bool Conditional()
    {
        foreach (int index in corners)
            if (null == gameModel[index].owner)
            {
                candidateIndex = index;
                return true;
            }

        return false;
    }

    public override bool Execute()
    {
        int x, y;
        gameModel.IndexToPosition(candidateIndex, out x, out y);

        Debug.Log(string.Format("EmptyCornerBehaviour.Execute(): ({0},{1})", x, y));

        return gameController.MakeMove(x, y);
    }
}
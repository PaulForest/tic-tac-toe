﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// If the player has two in a row, they can place a third to get three in a row.
/// -- https://en.wikipedia.org/wiki/Tic-tac-toe
/// </summary>
public class WinBehaviour : AbstractBehaviour
{
    int candidateIndex;

    public override bool Conditional()
    {
        List<int> candidates = AIHelper.FindWinningIndices(gameModel, player);
        if (candidates.Count > 0)
        {
            candidateIndex = candidates[0];
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool Execute()
    {
        int x, y;
        gameModel.IndexToPosition(candidateIndex, out x, out y);

        Debug.Log(string.Format("WinBehaviour.Execute(): ({0},{1})", x, y));

        return gameController.MakeMove(x, y);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A player marks the center. (If it is the first move of the game, playing
/// on a corner gives the second player more opportunities to make a mistake
/// and may therefore be the better choice; however, it makes no difference 
/// between perfect players.)
/// -- https://en.wikipedia.org/wiki/Tic-tac-toe
/// </summary>
public class CenterBehaviour : AbstractBehaviour
{
    public const int CenterIndex = 4, CenterX = 1, CenterY = 1;

    public override bool Conditional()
    {
        return (gameModel[CenterIndex].owner == null);
    }

    public override bool Execute()
    {
        Debug.Log(string.Format("CenterBehaviour.Execute(): x={0} y={1}", CenterX, CenterY));

        return gameController.MakeMove(CenterX, CenterY);
    }
}
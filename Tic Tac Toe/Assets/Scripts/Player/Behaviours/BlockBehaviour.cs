﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// If the opponent has two in a row, the player must play the third themselves to block the opponent.
/// -- https://en.wikipedia.org/wiki/Tic-tac-toe
/// </summary>
public class BlockBehaviour :  AbstractBehaviour
{
    int candidateIndex;

    public override bool Conditional()
    {
        List<int> candidates = AIHelper.FindWinningIndices(gameModel, gameModel.OtherPlayer);
        if (candidates.Count > 0)
        {
            // Keep the determined value.
            // Just for variety, choose one of the results at random.
            candidateIndex = candidates[Random.Range(0, candidates.Count)];
            return true;
        } else
        {
            return false;
        }
    }

    public override bool Execute()
    {
        int x, y;
        gameModel.IndexToPosition(candidateIndex, out x, out y);

        Debug.Log(string.Format("BlockBehaviour.Execute(): ({0},{1})", x, y));

        // Actually make the move via the controller
        return gameController.MakeMove(x, y);
    }
}
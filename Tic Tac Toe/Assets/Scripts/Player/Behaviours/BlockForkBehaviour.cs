﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  If there is only one possible fork for the opponent, the player should block it.
///  
///  Otherwise, the player should block any forks in any way that simultaneously allows them to create
///  two in a row. 
///  
///  Otherwise, the player should create a two in a row to force the opponent into defending, 
///  as long as it doesn't result in them creating a fork. 
///  
///  For example, if "X" has two opposite corners and "O" has the center, 
///  "O" must not play a corner in order to win. 
///  (Playing a corner in this scenario creates a fork for "X" to win.)
///  -- https://en.wikipedia.org/wiki/Tic-tac-toe#Strategy
/// </summary>
public class BlockForkBehaviour : AbstractBehaviour
{
    protected int candidateIndex;
    public override bool Conditional()
    {
        // We'll look at the opponent here
        AbstractPlayer otherPlayer = gameModel.OtherPlayer;

        // If there is only one possible fork for the opponent, the player should block it.
        List<int> opponentForkIndices = AIHelper.FindForks(gameModel, otherPlayer);
        if (1 == opponentForkIndices.Count)
        {
            candidateIndex = opponentForkIndices[0];
            return true;
        }

        // Otherwise, the player should block any forks in any way that simultaneously allows them 
        // to create two in a row. 
        if (opponentForkIndices.Count > 1)
        {
            List<int> result = new List<int>();
            foreach (int index in opponentForkIndices)
            {
                List<List<int>> blockForksAndCreateTwoInARowCandidates = AIHelper.WinningIndices.FindAll(list =>
                    list.Contains(index)
                    && list.FindAll(i => gameModel[i].owner == player).Count == 1
                    && list.FindAll(i => gameModel[i].owner == otherPlayer).Count == 0
                    && list.FindAll(i => gameModel[i].owner == null).Count == 1
                );

                if (blockForksAndCreateTwoInARowCandidates.Count > 0)
                {
                    // Keep this result
                    result.Add(index);
                }
            }

            if (result.Count> 0)
            {
                // Ok, use this value.
                // Just for variety, choose a random one.
                candidateIndex = result[Random.Range(0, result.Count)];
                return true;
            }
        }

        ///  Otherwise, the player should create a two in a row to force the opponent into defending, 
        ///  as long as it doesn't result in them creating a fork. 
        {
            List<List<int>> twoInARowCandidates = AIHelper.WinningIndices.FindAll(list =>
                list.FindAll(i => gameModel[i].owner == player).Count == 1
                && list.FindAll(i => gameModel[i].owner == otherPlayer).Count == 0
                && list.FindAll(i => gameModel[i].owner == null).Count == 1);

            // Ok, would this create a fork for the opponent?
            List<int> result = new List<int>();
            foreach (List<int> candidate in twoInARowCandidates)
            {
                foreach (int opponentForkIndex in opponentForkIndices)
                {
                    if (!candidate.Contains(opponentForkIndex))
                    {
                        result.Add(candidate[Random.Range(0, twoInARowCandidates.Count)]);
                    }
                }
            }
            if (result.Count > 0)
            {
                candidateIndex = Random.Range(0, result.Count);
                return true;
            }
        }

        return false;
    }

    public override bool Execute()
    {
        int x, y;
        gameModel.IndexToPosition(candidateIndex, out x, out y);

        Debug.Log(string.Format("BlockForkBehaviour.Execute(): ({0},{1})", x, y));

        return gameController.MakeMove(x, y);
    }
}
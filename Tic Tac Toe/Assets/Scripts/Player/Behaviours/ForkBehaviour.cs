﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Create an opportunity where the player has two threats to win (two 
/// non-blocked lines of 2).
/// -- https://en.wikipedia.org/wiki/Tic-tac-toe
/// </summary>
public class ForkBehaviour : AbstractBehaviour
{
    int candidateIndex;

    public override bool Conditional()
    {
        List<int> candidates = AIHelper.FindForks(gameModel, player);
        if (candidates.Count > 0)
        {
            candidateIndex = candidates[0];
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool Execute()
    {
        int x, y;
        gameModel.IndexToPosition(candidateIndex, out x, out y);

        Debug.Log(string.Format("ForkBehaviour.Execute(): x={0} y={1}", x, y));

        return gameController.MakeMove(x, y);
    }
}
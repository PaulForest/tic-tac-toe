﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// If the opponent is in the corner, the player plays the opposite corner.
/// -- https://en.wikipedia.org/wiki/Tic-tac-toe
/// </summary>
public class OppositeCornerBehaviour : AbstractBehaviour
{
    int candidateIndex;

    public override bool Conditional()
    {
        AbstractPlayer otherPlayer = gameModel.OtherPlayer;

        if (gameModel[0].owner == otherPlayer && gameModel[8].owner == null)
        {
            candidateIndex = 8;
            return true;
        }
        if (gameModel[8].owner == otherPlayer && gameModel[0].owner == null)
        {
            candidateIndex = 0;
            return true;
        }
        if (gameModel[2].owner == otherPlayer && gameModel[6].owner == null)
        {
            candidateIndex = 0;
            return true;
        }
        if (gameModel[6].owner == otherPlayer && gameModel[2].owner == null)
        {
            candidateIndex = 2;
            return true;
        }

        return false;
    }

    public override bool Execute()
    {
        int x, y;
        gameModel.IndexToPosition(candidateIndex, out x, out y);

        Debug.Log(string.Format("OppositeCornerBehaviour.Execute(): ({0},{1})", x, y));

        return gameController.MakeMove(x, y);
    }
}
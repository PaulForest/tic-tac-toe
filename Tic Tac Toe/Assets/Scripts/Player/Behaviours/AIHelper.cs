﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Some helpful data and methods to enable higher-level AI code.
/// </summary>
public static class AIHelper
{
    /// <summary>
    /// A list of all the ways the player can win.  Each inner list enumerates the
    /// indices in a gameModel instance that correspond with a winning three-in-a-row
    /// combination.  
    /// Created with lazy instantiation (only created on-demand)
    /// </summary>
    public static List<List<int>> WinningIndices
    {
        get
        {
            // lazy instantiation
            if (null == _winningIndices)
            {
                _winningIndices = new List<List<int>>(new List<int>[] {
                    new List<int>(new int[] { 0, 1, 2 }),
                    new List<int>(new int[] { 3, 4, 5 }),
                    new List<int>(new int[] { 6, 7, 8 }),
                    new List<int>(new int[] { 0, 3, 6 }),
                    new List<int>(new int[] { 1, 4, 7 }),
                    new List<int>(new int[] { 2, 5, 8 }),
                    new List<int>(new int[] { 0, 4, 8 }),
                    new List<int>(new int[] { 2, 4, 6 })
                });
            }
            return _winningIndices;
        }
    }

    /// <summary>
    /// Finds all the ways the player can win in one move on the particular board.
    /// You can tweak the desiredPlayerOwnedCount and desiredUnownedCount to find 
    /// particular combinations of tiles that are owned by the particular player or 
    /// are unowned.
    /// </summary>
    /// <param name="gameModel">the game data to inspect</param>
    /// <param name="forPlayer">the player to evaluate on this board</param>
    /// <param name="desiredPlayerOwnedCount">The desired number of existing tiles that are owned by the player. 
    /// Range is [0 .. GameModel.ConsecutiveTilesRequiredForWin]</param>
    /// <param name="desiredUnownedCount">The desired number of tiles that are unowned.  
    /// Range is [1 .. GameModel.ConsecutiveTilesRequiredForWin]</param>
    /// <returns>A list of all the indices that the given player could play to win the game</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">if desiredPlayerOwnedCount or desiredUnownedCount are 
    /// not within their specified ranges; or if the player or gameModel is invalid</exception>
    public static List<int> FindWinningIndices(GameModel gameModel, AbstractPlayer forPlayer, int desiredPlayerOwnedCount = 2, int desiredUnownedCount = 1)
    {
        // Sanity check parameters and barf exceptions if there's anything wrong
        if (null == gameModel)
        {
            throw new System.ArgumentOutOfRangeException();
        }

        if (forPlayer != gameModel.XPlayer && forPlayer != gameModel.OPlayer)
        {
            throw new System.ArgumentOutOfRangeException("That player isn't playing this game!");
        }

        if (desiredPlayerOwnedCount < 0 || desiredPlayerOwnedCount > GameModel.ConsecutiveTilesRequiredForWin)
        {
            throw new System.ArgumentOutOfRangeException(string.Format(
                "desiredPlayerOwnedCount must be within range [{0} .. {1})", 
                0, GameModel.ConsecutiveTilesRequiredForWin));
        }

        if (desiredUnownedCount < 1 || desiredUnownedCount > GameModel.ConsecutiveTilesRequiredForWin)
        {
            throw new System.ArgumentOutOfRangeException(string.Format(
                "desiredUnownedCount must be within range [{0} .. {1})",
                1, GameModel.ConsecutiveTilesRequiredForWin));
        }

        List<int> result = new List<int>();

        // Walk through all the ways it's possible to win the game.
        // We'll inspect each one to consider if it passes our criteria, and if it does, add that index
        // to the results.
        foreach (List<int> listToConsider in WinningIndices)
        {
            List<int> stuffIOwn = listToConsider.FindAll(index => forPlayer == gameModel[index].owner);
            List<int> stuffNoOneOwns = listToConsider.FindAll(index => null == gameModel[index].owner);

            if (stuffIOwn.Count == desiredPlayerOwnedCount
                && stuffNoOneOwns.Count == desiredUnownedCount)
            {
                // It's possible for the player to win with this index
                result.Add(stuffNoOneOwns[0]);
            }
        }
        return result;
    }

    /// <summary>
    /// Finds all the ways this player can win the game.  Returns a list of sequence of indices.
    /// Each sequence is a list of all the tiles needed to win the game.
    /// Very useful for AI.
    /// </summary>
    /// <param name="gameModel">the game model instance</param>
    /// <param name="forPlayer">the player to consider</param>
    /// <param name="desiredPlayerOwnedCount">the number of tiles required to be owned
    /// by this player in a given winning row.
    /// Range is [0 .. GameModel.ConsecutiveTilesRequiredForWin]</param>
    /// <param name="desiredUnownedCount">the number of tiles that must be unowned in a 
    /// particular sequence.
    /// Range is [1 .. GameModel.ConsecutiveTilesRequiredForWin]</param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentOutOfRangeException">if the player or gameModel is invalid</exception>
    public static List<List<int>> FindWinningSequences(GameModel gameModel, AbstractPlayer forPlayer, int desiredPlayerOwnedCount = 2, int desiredUnownedCount = 1)
    {
        // Sanity check parameters and barf exceptions if there's anything wrong
        if (null == gameModel)
        {
            throw new System.ArgumentOutOfRangeException();
        }

        if (forPlayer != gameModel.XPlayer && forPlayer != gameModel.OPlayer)
        {
            throw new System.ArgumentOutOfRangeException("That player isn't playing this game!");
        }

        if (desiredPlayerOwnedCount <= 0 || desiredPlayerOwnedCount > GameModel.ConsecutiveTilesRequiredForWin)
        {
            throw new System.ArgumentOutOfRangeException(string.Format(
                "desiredPlayerOwnedCount ({0}) must be within range [{1} .. {2})",
                desiredPlayerOwnedCount, 0, GameModel.ConsecutiveTilesRequiredForWin));
        }

        if (desiredUnownedCount < 0 || desiredUnownedCount > GameModel.ConsecutiveTilesRequiredForWin)
        {
            throw new System.ArgumentOutOfRangeException(string.Format(
                "desiredUnownedCount ({0}) must be within range [{1} .. {2})",
                desiredUnownedCount, 0, GameModel.ConsecutiveTilesRequiredForWin));
        }

        List<List<int>> result = new List<List<int>>();

        // Walk through all the ways it's possible to win the game.
        // We'll inspect each one to consider if it passes our criteria, and if it does, add that 
        // *entire sequence* to the results, not just the winning index.
        foreach (List<int> listToConsider in WinningIndices)
        {
            List<int> stuffIOwn = listToConsider.FindAll(index => forPlayer == gameModel[index].owner);
            List<int> stuffNoOneOwns = listToConsider.FindAll(index => null == gameModel[index].owner);

            if (stuffIOwn.Count == desiredPlayerOwnedCount
                && stuffNoOneOwns.Count == desiredUnownedCount)
            {
                result.Add(listToConsider);
            }
        }
        return result;
    }

    /// <summary>
    /// Finds all the ways that forks could be created in the board for the given player.
    /// So a fork is a way to be able to win in two different ways on your next turn.  The opponent
    /// can only block one of them, so forking is a great way to win.
    /// </summary>
    /// <param name="gameModel"></param>
    /// <param name="forPlayer"></param>
    /// <returns></returns>
    /// /// <exception cref="System.ArgumentOutOfRangeException">if the player or gameModel is invalid</exception>
    public static List<int> FindForks(GameModel gameModel, AbstractPlayer forPlayer)
    {
        // Sanity check parameters and barf exceptions if there's anything wrong
        if (null == gameModel)
        {
            throw new System.ArgumentOutOfRangeException();
        }

        if (forPlayer != gameModel.XPlayer && forPlayer != gameModel.OPlayer)
        {
            throw new System.ArgumentOutOfRangeException("That player isn't playing this game!");
        }

        List<int> result = new List<int>();

        // Find all sequences that:
        // The player owns exactly one tile
        // There are two unowned tiles.
        List<List<int>> candidates = FindWinningSequences(gameModel, forPlayer, 1, 2);

        // Compare the list of candidate sequences.  What we're looking for is the same index that exists
        // in multiple lists of candidates.  If we do find such an index, add to the list of results.
        foreach (List<int> candidate in candidates)
        {
            foreach (List<int> candidate2 in candidates)
            {
                if (candidate == candidate2)
                {
                    break;
                }

                foreach (int index in candidate)
                {
                    if (candidate2.Contains(index) && gameModel[index].owner == null)
                    {
                        result.Add(index);
                    }
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Local list of all the ways the game can be won.
    /// </summary>
    private static List<List<int>> _winningIndices;
}

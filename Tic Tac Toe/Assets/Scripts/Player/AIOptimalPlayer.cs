﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A perfect Tic-Tac-Toe player.  Not that much fun to play against.
/// Tends to get ties with itself.
/// </summary>
public class AIOptimalPlayer : AIBehaviourPlayer
{
    void Awake()
    {
        behaviours.Add(gameObject.AddComponent<WinBehaviour>());
        behaviours.Add(gameObject.AddComponent<BlockBehaviour>());
        behaviours.Add(gameObject.AddComponent<ForkBehaviour>());
        behaviours.Add(gameObject.AddComponent<BlockForkBehaviour>());
        behaviours.Add(gameObject.AddComponent<CenterBehaviour>());
        behaviours.Add(gameObject.AddComponent<OppositeCornerBehaviour>());
        behaviours.Add(gameObject.AddComponent<EmptyCornerBehaviour>());
        behaviours.Add(gameObject.AddComponent<EmptySideBehaviour>());
    }
}
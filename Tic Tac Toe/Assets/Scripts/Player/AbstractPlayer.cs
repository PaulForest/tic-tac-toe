﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A Tic-Tac-Toe player.  Has a letter, a type (AI or human) and can play a turn.
/// Subclass to implement OnTurnStart() when do something when it's your turn.
/// </summary>
[SerializeField]
public abstract class AbstractPlayer : MonoBehaviour
{
    public GameController gameController;
    public GameModel gameModel;

    /// <summary>
    /// Human or robot?
    /// </summary>
    public enum PlayerTypeDef
    {
        Human,
        AI
    }

    /// <summary>
    /// Override this in subclasses to know what kind of player this is.
    /// </summary>
    public abstract PlayerTypeDef PlayerType { get; }

    public enum PlayerLetter
    {
        X, O
    }
    public PlayerLetter playerLetter;

    /// <summary>
    /// Resets the instance such that it's the same as if we created this from
    /// scratch.
    /// Reinitializes all data including its gameModel and gameView instances.
    /// </summary>
    public abstract void Reset();

    /// <summary>
    /// Describe this instance
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        // Fun new thing I learned: the enum's item's identifer (X or O here) can
        // be used as a string.
        return string.Format("Player {0}", playerLetter);
    }

    protected virtual void Start()
    {
        OnEnable();
    }

    /// <summary>
    /// Override this method to do something on your turn.
    /// You can assume that it's your turn, the game is not over,
    /// and there exists at least one valid unowned tile.
    /// </summary>
    protected abstract void OnTurnStart();

    /// <summary>
    /// Listen for when it's your turn
    /// </summary>
    protected virtual void OnEnable()
    {
        if (null != gameController)
        {
            gameController.OnStartTurnEvent += GameController_OnTurnStartEvent;
        }
    }

    /// <summary>
    /// Clean up
    /// </summary>
    protected virtual void OnDisable()
    {
        gameController.OnStartTurnEvent -= GameController_OnTurnStartEvent;
    }

    /// <summary>
    /// If this is our turn and the game is still going, call OnTurnStart().
    /// </summary>
    /// <param name="player"></param>
    protected void GameController_OnTurnStartEvent(AbstractPlayer player)
    {
        if (gameModel.IsGameOver)
        {
            return;
        }

        if (this == player)
        {
            OnTurnStart();
        }
    }
}
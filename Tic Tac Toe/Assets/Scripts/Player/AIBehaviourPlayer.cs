﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// An AI agent that plays Tic-Tac-Toe.
/// Contains an ordered list of AbstractBehaviours that are evaluated one at time
/// via each Conditional() method.  The first one to return true is used, and its
/// Execute method is called.
/// Can also use a RandomBehaviour to randomly select a Tile.  Useful during debugging
/// when only some of the Behaviours are completed.
/// <see cref="AbstractBehaviour"/>
/// </summary>
public class AIBehaviourPlayer : AbstractPlayer
{
    public float fakeDelayMin = 0.1f, fakeDelayMax = 1.0f;

    /// <summary>
    /// The behaviours we'll use.  Note that the order is important as the first
    /// one to pass is used.
    /// </summary>
    public List<AbstractBehaviour> behaviours = new List<AbstractBehaviour>();

    /// <summary>
    /// If you want, you can set this to true to add an instance of RandomBehaviour to
    /// the end of this list (as long as no other instances already appear in the list).
    /// This ensures that at least some move is made, even if the conditionals of the
    /// real behaviours all return false.
    /// </summary>
    public bool AddRandomBehaviourAsFailsafe = true;

    public override PlayerTypeDef PlayerType
    {
        get
        {
            return PlayerTypeDef.AI;
        }
    }

    public override void Reset()
    {
    }

    /// <summary>
    /// If requested, adds a RandomBehaviour
    /// </summary>
    protected override void Start()
    {
        base.Start();

        // Add a failsafe random behaviour if we want one and don't already have one.
        // This prevents the AI from hanging if all behaviours fail their condition.
        if (AddRandomBehaviourAsFailsafe && behaviours.All(behaviour => !(behaviour is RandomBehaviour)))
        {
            behaviours.Add(gameObject.AddComponent<RandomBehaviour>());
        }
    }

    /// <summary>
    /// Walk through each AbstractBehaviour instance in order.  
    /// For each, call its conditional method.  If it returns true,
    /// execute it.  Otherwise move on to the next one.
    /// </summary>
    protected override void OnTurnStart()
    {
        StartCoroutine(_OnTurnStart());
    }

    protected IEnumerator _OnTurnStart()
    {
        foreach (AbstractBehaviour behaviour in behaviours)
        {
            if (behaviour.Conditional())
            {
                yield return new WaitForSeconds(Random.Range(fakeDelayMin, fakeDelayMax));
                if (behaviour.Execute())
                {
                    break;
                }
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFactory : MonoBehaviour
{
    [SerializeField]
    public GameController gamePrefab;

    public enum PlayerType
    {
        Human,
        AI_Optimal,
        AI_Random
    }

    public GameController CreateGameInstance(AbstractPlayer player1Prefab, AbstractPlayer player2Prefab, bool restartOnClick, bool restartAutomatically, GameStyle gameStyle)
    {
        GameController gameController = Instantiate(gamePrefab);

        AbstractPlayer player1 = Instantiate(player1Prefab, gameController.playerParent);
        AbstractPlayer player2 = Instantiate(player2Prefab, gameController.playerParent);

        gameController.AddPlayer(player1);
        gameController.AddPlayer(player2);

        if (restartOnClick)
        {
            gameController.AddGameResetOnClick();
        }

        if (restartAutomatically)
        {
            gameController.gameObject.AddComponent<RestartGameAutomatically>();
        }

        gameController.ApplyGameStyle(gameStyle);

        return gameController;
    }
}

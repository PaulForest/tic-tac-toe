﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// All the data about a particular game instance along with all the methods to manipulate it.
/// </summary>
public class GameModel : MonoBehaviour
{
    /// <summary>
    /// The dimentions of the board.
    /// </summary>
    public const int BoardWidth = 3, BoardHeight = 3, ConsecutiveTilesRequiredForWin = 3;

    public GameController gameController;

    public void AddGameResetOnClick()
    {
        StartCoroutine(_AddGameResetOnClick());
    }

    /// <summary>
    /// The actual game's data.  This is a mapping of the index of a tile (from 0-8) to GameBoardTile instances
    /// </summary>
    protected Dictionary<int, GameBoardTile> _tiles;

    /// <summary>
    /// The players
    /// </summary>
    protected AbstractPlayer _xPlayer, _oPlayer;
    protected AbstractPlayer _currentPlayer;

    /// <summary>
    /// The winner, if the game is already won
    /// </summary>
    protected AbstractPlayer _winner = null;

    /// <summary>
    /// The list of indices that won the game, if the game is already won
    /// </summary>
    protected List<int> _winningIndices = null;

    protected enum GameState
    {
        Playing, GameWon, GameTied
    }
    protected GameState _gameState = GameState.Playing;

    /// <summary>
    /// Set things up.
    /// Generally, set up this instance's internal state without depending on anything else's 
    /// internal state being set up yet.
    /// If particular references aren't set up in the inspector, attempt to find the references
    /// automatically.  This techniques makes it easier to use components that just work out of
    /// box.
    /// </summary>
    protected void Awake()
    {
        gameController = gameController ?? gameObject.GetComponent<GameController>();

        // Set up the game's data structures
        _tiles = new Dictionary<int, GameBoardTile>(BoardWidth * BoardHeight);
        int index = 0;
        for (int y = 0; y < GameModel.BoardHeight; y++)
        {
            for (int x = 0; x < GameModel.BoardWidth; x++)
            {
                GameBoardTile tile = new GameBoardTile
                {
                    x = x,
                    y = y,
                    owner = null,
                    uiTile = null
                };
                _tiles[index] = tile;

                index++;
            }
        }
    }

    protected IEnumerator Start()
    {
        yield return null;

        // If players aren't already linked in the inspector, attempt to find the players as children.
        if (null == _xPlayer || null == _oPlayer)
        {
            foreach (AbstractPlayer player in gameObject.GetComponentsInChildren<AbstractPlayer>())
            {
                if (null == _xPlayer && _oPlayer != player)
                {
                    _xPlayer = player;
                }

                if (null == _oPlayer && _xPlayer != player)
                {
                    _oPlayer = player;
                }

                if (null != _xPlayer && null != _oPlayer)
                {
                    break;
                }
            }
        }

        // If we're still not able to figure out the references to the players, then let the developer know.
        if (null == _xPlayer || null == _oPlayer)
        {
            Debug.LogError(string.Format("{0}.Awake(): players aren't configured and we can't sort it out automatically.  Bailing", this.name));
        }


        // Let the players know about relevant instances of GameController and GameModel
        _xPlayer.gameController = gameController;
        _xPlayer.gameModel = this;
        _oPlayer.gameController = gameController;
        _oPlayer.gameModel = this;

        // Set up the players
        _xPlayer.playerLetter = AbstractPlayer.PlayerLetter.X;
        _oPlayer.playerLetter = AbstractPlayer.PlayerLetter.O;
        _currentPlayer = _xPlayer;


        SwapPlayers();
    }

    /// <summary>
    /// Convenience method to get the index given the x, y tile coordinates.
    /// x and y is expected to be within the maximum size of the board as defined in BoardWidth and BoardHeight.
    /// Throws an exception if we're outside the valid range.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentOutOfRangeException">thrown when x, or y are outside
    /// the bounds of the board</exception>
    public int PositionToIndex(int x, int y)
    {
        if (x < 0 || x > GameModel.BoardWidth
            || y < 0 || y > GameModel.BoardHeight)
        {
            throw new System.ArgumentOutOfRangeException();
        }

        return y * BoardWidth + x;
    }

    /// <summary>
    /// Convenience method to get the x, y position given the index of a tile.
    /// index is expected to be within the maximum size of the board as defined in BoardWidth and BoardHeight.
    /// Throws an exception if we're outside the valid range.
    /// </summary>
    /// <param name="index"></param>
    /// <param name="x">must in range [0 .. BoardWidth]</param>
    /// <param name="y">must in range [0 .. BoardHeight]</param>
    /// /// <exception cref="System.ArgumentOutOfRangeException">thrown when index is outside
    /// the bounds of the board</exception>
    public void IndexToPosition(int index, out int x, out int y)
    {
        if (index < 0 || index > GameModel.BoardWidth * GameModel.BoardHeight)
        {
            throw new System.ArgumentOutOfRangeException();
        }

        x = index % BoardWidth;
        y = index / BoardHeight;
    }

    /// <summary>
    /// Makes a move for the specified player at the specified location.
    /// Validates that the move is possible: the tile must be empty.
    /// Returns false if it's not possible to make this move.
    /// </summary>
    /// <param name="player"></param>
    /// <param name="x">must in range [0 .. BoardWidth]</param>
    /// <param name="y">must in range [0 .. BoardHeight]</param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentOutOfRangeException">thrown when index is outside
    /// the bounds of the board</exception>
    public bool MakeMove(AbstractPlayer player, int x, int y)
    {
        int index = PositionToIndex(x, y);

        // make sure the tile is empty.  If it is, set to the specified player.
        if (null == _tiles[index].owner)
        {
            _tiles[index].owner = player;

            // Determine if the game has been won
            List<List<int>> winningSequences = AIHelper.FindWinningSequences(this, CurrentPlayer, 3, 0);
            if (winningSequences.Count > 0)
            {
                _winningIndices = new List<int>(winningSequences[0]);
                _winner = player;

                _gameState = GameState.GameWon;
            }

            // alternate players if the game isn't yet over
            if (!IsGameOver)
            {
                _currentPlayer = (_currentPlayer == _xPlayer) ? _oPlayer : _xPlayer;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Convenience method to quickly determine the owner of a specific location.
    /// Used by AI methods.
    /// </summary>
    /// <param name="x">must in range [0 .. BoardWidth]</param>
    /// <param name="y">must in range [0 .. BoardHeight]</param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentOutOfRangeException">thrown when index is outside
    /// the bounds of the board</exception>
    public AbstractPlayer GetOwnerForPosition(int x, int y)
    {
        int index = PositionToIndex(x, y);
        return _tiles[index].owner;
    }

    /// <summary>
    /// Resets all local data to an initialized state, as if we can created
    /// a new instance of this.
    /// </summary>
    public void Reset()
    {
        Debug.Log(string.Format("{0}.Reset()", name));

        foreach (GameBoardTile tile in _tiles.Values)
        {
            tile.owner = null;
        }
        XPlayer.Reset();
        OPlayer.Reset();

        SwapPlayers();
        _gameState = GameState.Playing;
    }

    public bool AddPlayer(AbstractPlayer player)
    {
        if (null == _xPlayer)
        {
            _xPlayer = player;
            _xPlayer.gameController = gameController;
            _xPlayer.gameModel = this;
            _xPlayer.playerLetter = AbstractPlayer.PlayerLetter.X;
            return true;
        }
        else if (null == _oPlayer)
        {
            _oPlayer = player;
            _oPlayer.gameController = gameController;
            _oPlayer.gameModel = this;
            _oPlayer.playerLetter = AbstractPlayer.PlayerLetter.O;
            return true;
        }
        else
        {
            return false;
        }
    }

    public List<TileUI> GetTileUIList()
    {
        return
            (from tile in _tiles
             select tile.Value.uiTile)
            .ToList<TileUI>();
    }

    /// <summary>
    /// Determines if this tile is part of this instance
    /// </summary>
    /// <param name="tile"></param>
    /// <returns>true iff the tile is part of this instance</returns>
    public bool IsTileValid(GameBoardTile tile)
    {
        return _tiles.Values.Contains<GameBoardTile>(tile);
    }

    /// <summary>
    /// Returns true if the game has either been won or tied
    /// </summary>
    public bool IsGameOver
    {
        get { return IsGameTied || IsGameWon; }
    }

    /// <summary>
    /// Returns true is the game has been tied / is a draw, meaning there are
    /// no more available moves to play.
    /// </summary>
    public bool IsGameTied
    {
        get
        {
            return _tiles.All(tile => null != tile.Value.owner);
        }
    }

    /// <summary>
    /// Convenience method to get a particular tile via its index.
    /// </summary>
    /// <param name="key">must in range [0 .. BoardWidth * BoardHeight]</param>
    /// <returns>the particular GameBoardTile for the given index</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">thrown when index is outside
    /// the bounds of the board</exception>
    public GameBoardTile this[int key]
    {
        get
        {
            if (key > _tiles.Count)
            {
                throw new System.ArgumentOutOfRangeException();
            }
            return _tiles[key];
        }
    }

    /// <summary>
    /// The player whose turn it is now
    /// </summary>
    public AbstractPlayer CurrentPlayer { get { return _currentPlayer; } }

    /// <summary>
    /// The player whose turn it is not right now
    /// </summary>
    public AbstractPlayer OtherPlayer { get { { return (_currentPlayer == _xPlayer) ? _oPlayer : _xPlayer; } } }

    /// <summary>
    /// The X Player
    /// </summary>
    public AbstractPlayer XPlayer { get { return _xPlayer; } }

    /// <summary>
    /// The O Player
    /// </summary>
    public AbstractPlayer OPlayer { get { return _oPlayer; } }

    /// <summary>
    /// Determines if the game has been won already.
    /// </summary>
    public bool IsGameWon
    {
        get
        {
            return _gameState == GameState.GameWon;
        }
    }

    /// <summary>
    /// If there's a winner, return it, otherwise return null;
    /// </summary>
    public AbstractPlayer Winner
    {
        get
        {
            return _winner;
        }
    }

    /// <summary>
    /// If the game has already been won, return the list of indices that won
    /// the game.  Otherwise return null.
    /// </summary>
    public List<int> WinningIndices
    {
        get
        {
            return _winningIndices;
        }
    }

    protected void SwapPlayers()
    {
        // 50% of the time, swap the players
        if (Random.Range(0, 2) == 0)
        {
            Debug.Log("Swapping players");
            AbstractPlayer temp = _xPlayer;
            _xPlayer = _oPlayer;
            _oPlayer = temp;

            _xPlayer.playerLetter = AbstractPlayer.PlayerLetter.X;
            _oPlayer.playerLetter = AbstractPlayer.PlayerLetter.O;

            _currentPlayer = _xPlayer;
        }
    }

    protected IEnumerator _AddGameResetOnClick()
    {
        yield return null;

        foreach (GameBoardTile tile in _tiles.Values)
        {
            if (null != tile.uiTile)
            {
                tile.uiTile.gameObject.AddComponent<RestartGameOnClick>();
            }
        }
    }
}
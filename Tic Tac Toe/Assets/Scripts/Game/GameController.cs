﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the GameModel instance in response to user and AI actions.
/// Publishes events about changes in the game: starting and ending the game, playing moves, etc.
/// </summary>
[RequireComponent(typeof(GameModel), typeof(GameView))]
public class GameController : MonoBehaviour
{
    public GameModel gameModel;
    public GameView gameView;

    /// <summary>
    /// We'll attach newly created Players to this to keep things clean
    /// </summary>
    public Transform playerParent;

    #region Events All the events we publish about the flow of the game.

    public delegate void OnGameStart();
    /// <summary>
    /// Dispatched when the game starts
    /// </summary>
    public OnGameStart OnGameStartEvent;

    public delegate void OnStartTurn(AbstractPlayer player);
    /// <summary>
    /// Dispatched when the specified player's turn starts
    /// </summary>
    public OnStartTurn OnStartTurnEvent;

    public delegate void OnTurnPlayed(AbstractPlayer player, int index);
    /// <summary>
    /// Dispatched when the specified player has successfully played at the specified index
    /// </summary>
    public OnTurnPlayed OnTurnPlayedEvent;

    public delegate void OnGameWon(AbstractPlayer winner);
    /// <summary>
    /// Dispatched when the specified player has just won the game
    /// </summary>
    public OnGameWon OnGameWonEvent;

    public delegate void OnGameTied();
    /// <summary>
    /// Dispatched when the game is a tie
    /// </summary>
    public OnGameTied OnGameTiedEvent;

    public delegate void OnGameStyleChanged(GameStyle gameStyle);
    /// <summary>
    /// Dispatched when the game's style is changed
    /// </summary>
    public OnGameStyleChanged OnGameStyleChangedEvent;

    #endregion

    /// <summary>
    /// Executes a move by the current player at a given position.
    /// Positions are zero-based using a left-handed coordinate system.
    /// Will not allow an invalid move to be made (i.e. x, y are out of bounds,
    /// that tile is already taken, etc.  Returns false if there's a problem.
    /// Dispatches events about this play: ending a turn, winning or tying the game
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns>true iff the move can be completely executed</returns>
    public bool MakeMove(int x, int y)
    {
        AbstractPlayer player = gameModel.CurrentPlayer;

        if (!gameModel.MakeMove(player, x, y))
        {
            return false;
        }

        StartCoroutine(_MakeMove(player, x, y));

        return true;
    }

    /// <summary>
    /// Determines if this tile is part of this instance
    /// </summary>
    /// <param name="tile"></param>
    /// <returns>true iff the tile is part of this instance</returns>
    public bool IsTileValid(GameBoardTile tile)
    {
        return gameModel.IsTileValid(tile);
    }

    /// <summary>
    /// Resets the instance such that it's the same as if we created this from
    /// scratch.
    /// Reinitializes all data including its gameModel and gameView instances.
    /// </summary>
    public void Reset()
    {
        gameModel.Reset();
        gameView.Reset();

        StartCoroutine(Start());
    }

    /// <summary>
    /// Adds a new player to the game.  You can only have two players
    /// </summary>
    /// <param name="player"></param>
    /// <returns>true iff the player could be added</returns>
    public bool AddPlayer(AbstractPlayer player)
    {
        return gameModel.AddPlayer(player);
    }

    public void AddGameResetOnClick()
    {
        gameModel.AddGameResetOnClick();
    }

    public void ApplyGameStyle(GameStyle gameStyle)
    {
        if (null != OnGameStyleChangedEvent)
        {
            OnGameStyleChangedEvent(gameStyle);
        }
    }

    /// <summary>
    /// Dispatches the relevant events. 
    /// We use a coroutine with yielding one frame so that all event listeners can complete their
    /// execution before we continue.  
    /// </summary>
    /// <param name="player"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="move"></param>
    /// <returns></returns>
    protected IEnumerator _MakeMove(AbstractPlayer player, int x, int y)
    {
        yield return null;

        if (OnTurnPlayedEvent != null)
        {
            OnTurnPlayedEvent(player, gameModel.PositionToIndex(x, y));
        }

        if (gameModel.IsGameWon)
        {
            Debug.Log(string.Format("{0}._MakeMove(): player {1} won!", name, player));
       
            if (null != OnGameWonEvent)
            {
                OnGameWonEvent(gameModel.Winner);
            }
        }
        else if (gameModel.IsGameTied)
        {
            if (null != OnGameTiedEvent)
            {
                OnGameTiedEvent();
            }
        }
        else
        {
            StartTurn(gameModel.CurrentPlayer);
        }
    }

    /// <summary>
    /// Starts the game flow.
    /// </summary>
    /// <returns></returns>
    protected IEnumerator Start()
    {
        gameModel = gameModel ?? GetComponent<GameModel>();
        gameView = gameView ?? GetComponent<GameView>();

        // Wait a frame just so everything is loaded and wired up.
        yield return null;

        if (null != OnGameStartEvent)
        {
            OnGameStartEvent();
        }

        yield return null;
        yield return new WaitForSeconds(1);

        StartTurn(gameModel.CurrentPlayer);
    }

    /// <summary>
    /// Dispatches the event that it's the specified player's turn
    /// </summary>
    /// <param name="player"></param>
    protected void StartTurn(AbstractPlayer player)
    {
        if (OnStartTurnEvent != null)
        {
            OnStartTurnEvent(gameModel.CurrentPlayer);
        }
    }
}
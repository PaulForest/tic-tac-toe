﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The view of the GameModel.
/// </summary>
public class GameView : MonoBehaviour
{
    /// <summary>
    /// These need to be set up in the inspector, but they can't be found we'll 
    /// attempt to set them automatically
    /// </summary>
    public GameController gameController;
    public GameModel gameModel;

    /// <summary>
    /// The tiles used for the view.
    /// </summary>
    public List<TileUI> uiTiles = new List<TileUI>();

    /// <summary>
    /// The prefab to instantiate to draw winning lines when the game is won
    /// </summary>
    public GameObject winningLinePrefab;

    public GameObject background;

    protected GameObject winningLineInstance;

    /// <summary>
    /// Resets all local data to an initialized state, as if we can created
    /// a new instance of this.
    /// </summary>
    public void Reset()
    {
        if (null != winningLineInstance)
        {
            Destroy(winningLineInstance);
        }

        foreach (TileUI tileUI in uiTiles)
        {
            tileUI.MarkAsDirty();
        }
    }

    public void ApplyGameStyle(GameStyle gameStyle)
    {
        StartCoroutine(_ApplyGameStyle(gameStyle));
    }

    protected IEnumerator _ApplyGameStyle(GameStyle gameStyle)
    {
        yield return null;

        foreach (TileUI tileui in gameModel.GetTileUIList())
        {
            tileui.ApplyGameStyle(gameStyle);
        }

        background.GetComponent<SpriteRenderer>().sprite = gameStyle.spriteAtlas.GetSprite("Board");
    }

    /// <summary>
    /// Initialize this instance.
    /// </summary>
    protected void Start()
    {
        // if these aren't already set up in the inspector, attempt to find them.
        if (null == gameModel)
        {
            gameModel = GetComponent<GameModel>();
        }
        if (null == gameController)
        {
            gameController = GetComponent<GameController>();
        }
        if (null == gameModel || null == gameController)
        {
            Debug.LogError(string.Format("{0}.Start(): please configure GameModel and GameController in the inspector ", this.name));
        }

        // Initialize the tiles.  They have to be existing instances that are configured in the inspector.
        int expectedTileCount = GameModel.BoardWidth * GameModel.BoardHeight;
        if (uiTiles.Count > 0 && uiTiles.Count != expectedTileCount)
        {
            Debug.LogError(string.Format("{0}.Start(): please configure uiTiles in the inspector", this.name));
            return;
        }

        // Set up the references from each GameUI tile instance to the corresponding GameBoardTile in the GameModel.
        int max = GameModel.BoardWidth * GameModel.BoardHeight;
        for (int i = 0; i < max; i++)
        {
            uiTiles[i].gameTile = gameModel[i];
            uiTiles[i].gameController = gameController;
            gameModel[i].uiTile = uiTiles[i];
        }
    }

    /// <summary>
    /// Update the UI when the game changes.
    /// </summary>
    protected void OnEnable()
    {
        gameController.OnTurnPlayedEvent += OnTurnPlayed;
        gameController.OnGameWonEvent += OnGameWon;
        gameController.OnGameStyleChangedEvent += OnGameStyleChanged;
    }

    /// <summary>
    /// Clean up when we're disabled
    /// </summary>
    protected void OnDisable()
    {
        gameController.OnTurnPlayedEvent -= OnTurnPlayed;
        gameController.OnGameWonEvent -= OnGameWon;
        gameController.OnGameStyleChangedEvent -= OnGameStyleChanged;
    }

    /// <summary>
    /// Set the TileUI to be have the correct owner
    /// </summary>
    /// <param name="player"></param>
    /// <param name="index"></param>
    protected void OnTurnPlayed(AbstractPlayer player, int index)
    {
        if (uiTiles.Count >= index)
        {
            uiTiles[index].MarkAsDirty();
        }
    }

    /// <summary>
    /// Draw a line over the winning tiles.
    /// Instantiate an instance of winningLinePrefab and set its positions
    /// to points that are a little further out of the center of the furthest
    /// tiles.  
    /// </summary>
    /// <param name="winner"></param>
    protected void OnGameWon(AbstractPlayer winner)
    {
        winningLineInstance = Instantiate(winningLinePrefab, transform, true);

        // nudge this a little closer to the camera so the line can be seen
        Vector3 pos = winningLineInstance.transform.position;
        pos.z -= 0.1f;
        winningLineInstance.transform.position = pos;

        LineRenderer lineRenderer = winningLineInstance.GetComponent<LineRenderer>();
        List<Vector3> points = new List<Vector3>();

        Vector3 first = gameModel[gameModel.WinningIndices[0]].uiTile.transform.position;
        Vector3 second = gameModel[gameModel.WinningIndices[2]].uiTile.transform.position;

        // keep the direction the line is going
        Vector3 direction = (second - first).normalized;

        // determine the furthest we can go in either direction, assuming that the indexes are
        // increasing in value.
        Vector3 min = gameModel[gameModel.WinningIndices[0]].uiTile.GetComponent<Collider2D>().bounds.min;
        Vector3 max = gameModel[gameModel.WinningIndices[2]].uiTile.GetComponent<Collider2D>().bounds.max;

        // Push this closer to the edge of the tile's bounds
        points.Add(first - direction * (first - min).magnitude * .5f);
        points.Add(second + direction * (max - second).magnitude * .5f);

        // Update the line renderer to draw the line
        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPositions(points.ToArray());
    }

    protected void OnGameStyleChanged(GameStyle gameStyle)
    {
        gameStyle.ApplyGameStyle(this);
    }
}
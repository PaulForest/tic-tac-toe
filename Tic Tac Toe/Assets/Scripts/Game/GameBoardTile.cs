﻿/// <summary>
/// Data class representing a particular tile.  
/// 
/// </summary>
public class GameBoardTile
{
    /// <summary>
    /// We hold onto the coordinates to make UI easier later
    /// </summary>
    public int x, y;

    /// <summary>
    /// The player that has played on this tile, or null if no one
    /// has played here yet.
    /// </summary>
    public AbstractPlayer owner;

    /// <summary>
    /// The view of the particular tile
    /// </summary>
    public TileUI uiTile;
}

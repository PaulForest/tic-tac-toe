﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Allows you to set the layer in the inspector as a single-selection
/// drop-down list with all the named layers.  A LayerMask by comparison
/// allows for a bitmask of many layers, which isn't always what we want.
/// 
/// You'll also need Editor/LayerAttributeEditor.cs for this to work.
/// </summary>
/// <example>Use this syntax to allow a variable to appear in the inspector as
/// such a Layer selection:
/// 
/// [SerializeField, Layer]
/// int uiLayer;
/// 
/// and later bitshift the value like this:
/// Physics.Raycast(ray, out raycastHit, 100f, 1 << uiLayer)
/// </example>
public class LayerAttribute : PropertyAttribute
{

}


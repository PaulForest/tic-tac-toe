﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Just writes to each of the GameController's events to the console.
/// Useful for debugging.
/// </summary>
public class ObviousGuy : MonoBehaviour
{
    public GameController gameController;

    /// <summary>
    /// Subscribe to every event the GameController has and just dump out each one's contents.
    /// </summary>
    private void Start()
    {
        gameController.OnGameStartEvent += () => Debug.Log("Game started");
        gameController.OnStartTurnEvent += (AbstractPlayer player) => Debug.Log(string.Format("Turn started: player is: {0}", player));
        gameController.OnTurnPlayedEvent += (AbstractPlayer player, int index) => Debug.Log(string.Format("Played turn: player is: {0}, move is: {1}", player, index));
        gameController.OnGameTiedEvent += () => Debug.Log("Game tied!");
        gameController.OnGameWonEvent += (AbstractPlayer winner) => Debug.Log(string.Format("Game won! winner is: {0}", winner));
    }
}
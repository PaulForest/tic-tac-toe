﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Some uility helper methods.
/// </summary>
public static class Util
{
    /// <summary>
    /// Dumps out a generic List into a comma-separated list, using each item's
    /// ToString() method.
    /// Useful for debugging.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="separator"></param>
    /// <returns></returns>
    public static string ListToString<T>(List<T> list, string separator = ",")
    {
        string result = "";
        int count = list.Count;
        if (count > 0)
        {
            T last = list[count - 1];
            list.ForEach(delegate (T item)
            {
                if (!last.Equals(item))
                {
                    result += string.Format("{0}{1}", item.ToString(), separator);
                }
                else
                {
                    result += item.ToString();
                }
            });
        }
        return result;
    }
}
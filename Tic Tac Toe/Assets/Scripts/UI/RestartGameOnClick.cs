﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// When the game has finished, allow the user to restart the game by clicking
/// on the game board.
/// </summary>
public class RestartGameOnClick : MonoBehaviour
{
    /// <summary>
    /// The layer used for UI.  We'll raycast with this layer.  
    /// The inspector interface for this field uses the Layer names, rather than a LayerMask.
    /// This is made possible with Editor/LayerAttributeEditor.cs and the LayerAttribute class.
    /// <see cref="LayerAttribute"/>
    /// <see cref="LayerAttributeEditor"/>
    /// </summary>
    [SerializeField, Layer]
    public int uiLayer;

    private GameController gameController;

    /// <summary>
    /// Only check for clicks/taps when required
    /// </summary>
    private bool checkForClick = false;

    private void Start()
    {
        gameController = GetComponentInParent<GameController>();
        OnEnable();

        if (0 == uiLayer)
        {
            uiLayer = LayerMask.NameToLayer("UI");
        }
    }

    /// <summary>
    /// Subscribe to the game controller's game ending events.
    /// </summary>
    private void OnEnable()
    {
        if (gameController)
        {
            gameController.OnGameStartEvent += OnGameStart;

            gameController.OnGameTiedEvent += OnGameEnd;
            gameController.OnGameWonEvent += OnGameEnd2;
        }
    }

    /// <summary>
    /// Clean up
    /// </summary>
    private void OnDisable()
    {
        gameController.OnGameTiedEvent -= OnGameEnd;
        gameController.OnGameWonEvent -= OnGameEnd2;
    }

    private void OnGameEnd()
    {
        checkForClick = true;
    }

    private void OnGameEnd2(AbstractPlayer winner)
    {
        OnGameEnd();
    }

    private void OnGameStart()
    {
        checkForClick = false;
    }

    private void OnMouseDown()
    {
        if (!checkForClick)
        {
            return;
        }

        gameController.Reset();
        checkForClick = false;
    }
}

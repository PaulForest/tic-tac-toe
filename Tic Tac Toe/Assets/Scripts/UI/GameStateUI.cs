﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Updates the Text UI in the game based on in-game events.
/// </summary>
[RequireComponent(typeof(TextMeshPro))]
public class GameStateUI : MonoBehaviour
{
    public GameModel gameModel;
    public GameController gameController;

    /// <summary>
    /// The large text announcing the major state of the game i.e. "You Win!"
    /// </summary>
    public TextMeshProUGUI titleText;

    /// <summary>
    /// The minor state of the game i.e. "Player X's turn"
    /// </summary>
    public TextMeshProUGUI helpText;

    /// <summary>
    /// A longer text section explaining how to play
    /// </summary>
    public TextMeshProUGUI instructionText;

    public bool showTutorial = false;

    void Awake()
    {
        gameController = gameController ?? GetComponentInParent<GameController>();
        gameModel = gameModel ?? GetComponentInParent<GameModel>();
    }

    /// <summary>
    /// Subscribe to relevant events
    /// </summary>
    void OnEnable()
    {
        gameController.OnGameStartEvent += GameController_OnGameStartEvent;
        gameController.OnStartTurnEvent += GameController_OnStartTurnEvent;
        gameController.OnGameTiedEvent += GameController_OnGameTiedEvent;
        gameController.OnGameWonEvent += GameController_OnGameWonEvent;
    }

    /// <summary>
    /// Clean up
    /// </summary>
    void OnDisable()
    {
        gameController.OnGameStartEvent -= GameController_OnGameStartEvent;
        gameController.OnStartTurnEvent -= GameController_OnStartTurnEvent;
        gameController.OnGameTiedEvent -= GameController_OnGameTiedEvent;
        gameController.OnGameWonEvent -= GameController_OnGameWonEvent;
    }

    /// <summary>
    /// Update text when the game starts
    /// </summary>
    void GameController_OnGameStartEvent()
    {
        if (showTutorial)
        {
            titleText.SetText("Game Started!");
            instructionText.SetText("Click in the play space to put your mark.\nGet three in a row to win!");
            helpText.SetText("");
        }
        else
        {
            titleText.SetText("");
            instructionText.SetText("");
            helpText.SetText("");
        }
    }

    /// <summary>
    /// Update text when the turn starts
    /// </summary>
    /// <param name="player"></param>
    void GameController_OnStartTurnEvent(AbstractPlayer player)
    {
        titleText.SetText("");
        instructionText.SetText("");
        if (player.PlayerType == AbstractPlayer.PlayerTypeDef.Human)
        {
            helpText.SetText(string.Format("Your Turn, Player {0}", player.playerLetter));
        }
        else
        {
            helpText.SetText(string.Format("{0}'s Turn", player));
        }
    }

    /// <summary>
    /// Update text when the game is tied
    /// </summary>
    void GameController_OnGameTiedEvent()
    {
        titleText.SetText("Game tied!");
        instructionText.SetText("");
        helpText.SetText("");
    }

    /// <summary>
    /// Update text when the game is won
    /// </summary>
    /// <param name="winner"></param>
    void GameController_OnGameWonEvent(AbstractPlayer winner)
    {
        string title;
        if (winner.PlayerType == AbstractPlayer.PlayerTypeDef.Human)
        {
            title = "You Win!";
        }
        else
        {
            AbstractPlayer otherPlayer = (winner == gameModel.OPlayer) ? gameModel.XPlayer : gameModel.OPlayer;
            if (otherPlayer.PlayerType == AbstractPlayer.PlayerTypeDef.Human)
            {
                title = "You Lose!";
            }
            else
            {
                title = string.Format("{0} Wins!", winner);
            }
        }
        titleText.SetText(title);
        instructionText.SetText("");
        helpText.SetText("");
    }
}
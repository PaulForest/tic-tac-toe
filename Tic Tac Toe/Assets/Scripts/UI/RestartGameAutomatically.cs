﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// When the game has finished, allow the user to restart the game by clicking
/// on the game board.
/// </summary>
public class RestartGameAutomatically : MonoBehaviour
{
    public float delayBeforeRestarting = 2;

    private GameController gameController;

    private void Start()
    {
        gameController = GetComponentInParent<GameController>();
        OnEnable();
    }

    /// <summary>
    /// Subscribe to the game controller's game ending events.
    /// </summary>
    private void OnEnable()
    {
        if (gameController)
        {
            gameController.OnGameTiedEvent += OnGameEnd;
            gameController.OnGameWonEvent += OnGameEnd2;
        }
    }

    /// <summary>
    /// Clean up
    /// </summary>
    private void OnDisable()
    {
        gameController.OnGameTiedEvent -= OnGameEnd;
        gameController.OnGameWonEvent -= OnGameEnd2;
    }

    void OnGameEnd()
    {
        StartCoroutine(_ResetGame());
    }

    void OnGameEnd2(AbstractPlayer winner)
    {
        OnGameEnd();
    }

    IEnumerator _ResetGame()
    {
        yield return new WaitForSeconds(delayBeforeRestarting);
        gameController.Reset();
    }
}
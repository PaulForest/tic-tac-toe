﻿using UnityEngine;
using UnityEngine.U2D;
using System.Collections;
using UnityEngine.Networking;

public class GameStyle : MonoBehaviour
{
    public string assetBundleName;
    public string spriteAtlasName;
    
    public SpriteAtlas spriteAtlas;

    public void ApplyGameStyle(GameView gameView)
    {
        if (null == spriteAtlas)
        {
            StartCoroutine(LoadAndApplyGameStyle(gameView));
        }
        else
        {
            gameView.ApplyGameStyle(this);
        }
    }

    protected IEnumerator LoadAndApplyGameStyle(GameView gameView)
    {
        string uri = "file:///" + Application.dataPath + "/AssetBundles/" + assetBundleName;
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(uri);
        yield return request.SendWebRequest();
        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);

        spriteAtlas = bundle.LoadAsset<SpriteAtlas>(spriteAtlasName);

        gameView.ApplyGameStyle(this);
    }
}
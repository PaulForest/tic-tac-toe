﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The user interface for the tile.  Shows the current owner.
/// </summary>
public class TileUI : MonoBehaviour
{
    /// <summary>
    /// The GameController instance
    /// </summary>
    public GameController gameController;

    /// <summary>
    /// The related data tile
    /// </summary>
    public GameBoardTile gameTile;

    /// <summary>
    /// References to the different styles of selected states.
    /// Each states has its own GameObject to allow for maximum
    /// styling flexibility.
    /// </summary>
    public GameObject unselected, xSelected, oSelected;

    /// <summary>
    /// Refresh the UI to reflect the current contents of the tile.
    /// Call this when the gameTile has changed.
    /// </summary>
    public void MarkAsDirty()
    {
        if (null == gameTile.owner)
        {
            unselected.SetActive(true);
            xSelected.SetActive(false);
            oSelected.SetActive(false);
        }
        else
        {
            switch (gameTile.owner.playerLetter)
            {
                case AbstractPlayer.PlayerLetter.X:
                    unselected.SetActive(false);
                    xSelected.SetActive(true);
                    oSelected.SetActive(false);
                    break;

                case AbstractPlayer.PlayerLetter.O:
                    unselected.SetActive(false);
                    xSelected.SetActive(false);
                    oSelected.SetActive(true);
                    break;
            }
        }
    }

    public void ApplyGameStyle(GameStyle gameStyle)
    {
        xSelected.GetComponent<SpriteRenderer>().sprite = gameStyle.spriteAtlas.GetSprite("X-Mark");
        oSelected.GetComponent<SpriteRenderer>().sprite = gameStyle.spriteAtlas.GetSprite("O-Mark");
    }

    protected void OnMouseDown()
    {
        HumanPlayer humanPlayer = gameController.gameModel.CurrentPlayer as HumanPlayer;
        if (null == humanPlayer)
        {
            return;
        }

        if (gameController.gameModel.CurrentPlayer.PlayerType==AbstractPlayer.PlayerTypeDef.Human 
            && humanPlayer.IsListeningForInput
            && gameController.IsTileValid(gameTile)
            && gameController.MakeMove(gameTile.x, gameTile.y))
        {
            Debug.Log(string.Format("{0}.OnMouseDown() making move at ({1},{2})", name, gameTile.x, gameTile.y));

            humanPlayer.IsListeningForInput = false;
        }
    }

    /// <summary>
    /// Refresh the UI to reflect the current contents of the tile.
    /// </summary>
    protected IEnumerator Start()
    {
        yield return null;

        MarkAsDirty();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Shows all the UI for the game.  Enables and disabled child GameObjects
/// as needed for each part of the game.
/// </summary>
public class MainMenuUI : MonoBehaviour
{
    public GameFactory gameFactory;

    /// <summary>
    /// The child GameObjects we'll enable and disable
    /// </summary>
    public GameObject mainMenu;
    public GameObject customGameMenu;
    public GameObject inGameMenu;

    public GameStyle basicGameStyle, fancyGameStyle;

    /// <summary>
    /// Drop-downs for the types of players we can have
    /// </summary>
    public TMP_Dropdown player1Options, player2Options;
    
    /// <summary>
    /// A mapping of a display name for a type of player to a prefab.
    /// Set up in the inspector.
    /// </summary>
    [System.Serializable]
    public struct PlayerTypeToPrefab
    {
        public string name;
        public AbstractPlayer player;
    }
    public List<PlayerTypeToPrefab> playerTypeNameToPrefabMap = new List<PlayerTypeToPrefab>();

    protected List<GameController> gameInstances = new List<GameController>();

    /// <summary>
    /// Show the main menu, initialize the list of available player types
    /// </summary>
    public void Start()
    {
        gameFactory = gameFactory ?? GetComponent<GameFactory>();
        ShowMainMenu();

        player1Options.ClearOptions();
        player2Options.ClearOptions();

        List<string> names = new List<string>();
        int player1DefaultOption = 0, player2DefaultOption = 0;

        for (int i = 0; i < playerTypeNameToPrefabMap.Count; i++)
        {
            PlayerTypeToPrefab playerTypeToPrefab = (PlayerTypeToPrefab)playerTypeNameToPrefabMap[i];
            names.Add(playerTypeToPrefab.name);

            if (playerTypeToPrefab.player.PlayerType == AbstractPlayer.PlayerTypeDef.Human)
            {
                player1DefaultOption = i;
            } else
            {
                player2DefaultOption = i;
            }
        }

        player1Options.AddOptions(names);
        player1Options.value = player1DefaultOption;

        player2Options.AddOptions(names);
        player2Options.value = player2DefaultOption;
    }

    /// <summary>
    /// Destroys any game instances that are running and shows only the appropriate
    /// menu.
    /// </summary>
    public void ShowMainMenu()
    {
        KillAllGameInstances();

        mainMenu.SetActive(true);
        customGameMenu.SetActive(false);
        inGameMenu.SetActive(false);
    }

    public void StartSinglePlayer()
    {
        KillAllGameInstances();

        AbstractPlayer player1Prefab = playerTypeNameToPrefabMap.Find(t => t.player.PlayerType == AbstractPlayer.PlayerTypeDef.Human).player;
        AbstractPlayer player2Prefab = playerTypeNameToPrefabMap.FindLast(t => t.player.PlayerType == AbstractPlayer.PlayerTypeDef.AI).player;

        gameInstances.Add(gameFactory.CreateGameInstance(player1Prefab, player2Prefab, true, false, basicGameStyle));

        mainMenu.SetActive(false);
        customGameMenu.SetActive(false);
        inGameMenu.SetActive(true);
    }

    /// <summary>
    /// Destroys any game instances that are running and shows only the appropriate
    /// menu.
    /// Allows the user to select what kinds of players they want in the game: humans
    /// or various AI agents.
    /// </summary>
    public void ShowCustomOptions()
    {
        KillAllGameInstances();

        mainMenu.SetActive(false);
        customGameMenu.SetActive(true);
        inGameMenu.SetActive(false);
    }

    /// <summary>
    /// Destroys any game instances that are running and shows only the appropriate
    /// menu.
    /// Starts the game using the user's chosen player types.
    /// If there's at least one human player, we add the RestartGameOnClick component.
    /// Otherwise we add the RestartGameAutomatically component.
    /// </summary>
    public void StartCustomGame()
    {
        KillAllGameInstances();

        mainMenu.SetActive(false);
        customGameMenu.SetActive(false);
        inGameMenu.SetActive(true);

        AbstractPlayer player1Prefab = playerTypeNameToPrefabMap.Find(t => t.name == player1Options.options[player1Options.value].text).player;
        AbstractPlayer player2Prefab = playerTypeNameToPrefabMap.Find(t => t.name == player2Options.options[player2Options.value].text).player;

        bool restartOnClick = (player1Prefab is HumanPlayer || player2Prefab is HumanPlayer);

        gameInstances.Add(gameFactory.CreateGameInstance(player1Prefab, player2Prefab, restartOnClick, !restartOnClick, fancyGameStyle));
    }

    /// <summary>
    /// Destroys any game instances that are running and shows only the appropriate
    /// menu.
    /// </summary>
    protected void KillAllGameInstances()
    {
        while (gameInstances.Count > 0)
        {
            GameController gameController = gameInstances[0];
            gameInstances.Remove(gameController);
            Destroy(gameController.gameObject);
        }
    }
}
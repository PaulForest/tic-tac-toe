# Tic Tac Toe
## By Paul Forest

# Overview

This is a quick single-player implementation of the game Tic-Tac-Toe.  You can set up a game with any two of: humans, hard, medium or easy AI agents.  Selecting "Single Player" sets up one human player against one hard AI.

The hard AI agent satisfies the algorithm described on the Tic-Tac-Toe Wikipedia page, which is pretty much a perfect AI.

The project uses the Model-View-Controller paradigm for the main game class.
The AI uses a modular format for its behaviours, and you can set up an AI agent with as many or as few of these behaviours as desired to configure it.  The order in which the behaviours is assigned is important, as it will use the first behaviour that passes its criteria.  





